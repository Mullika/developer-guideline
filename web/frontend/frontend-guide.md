# Frontend Guideline

### This document is a standard coding for frontend developer of Nextzy.


## Language

Use Typescript and css style only for front-end development.

** Except old project still using vanilla js and scss but when create new project should be use typescript and css style.

>ยกเว้นบาง project ที่มีทำไว้แล้วอาจใช้  js และ scss แต่ project ที่ขึ้นใหม่ต้องใช้ Typescript และ css style


## Naming

### Casing

Names of types should be use `camelCase` only

Here’s the example;

``` 
-	camelCase
-	PascalCase
-	snake_case
-	kebab-case  
```  

>การตั้งชื่อตัวแปรควรตั้งชื่อให้สื่อความหมายและเข้าใจง่ายมากที่สุด และใช้รูปแบบการตั้งชื่อจะใช้เป็น `camelCase` เท่านั้น  

### Naming rules

-	Include all the words needed to avoid ambiguity for a person reading code where the name is use

	Preferred
	
	```js
	const isOpen = true;
	const fruitNames = ['apple', 'banana', 'cucumber'];
	function onRemoveFruitName() {
	// ..stuff..
	}
	```

	Not Preferred

	```js
	const open = true;
	const fruit = ['apple', 'banana', 'cucumber'];
	function onRemove() {
	// ...stuff...
	} // unclear: are we removing?
	```

-	Package Naming 
	
	-   [Angular](./angular-guide.md)
	-   [ReactJS](./react-guide.md)
	-   [VueJS](./vue-guide.md) **ยังไม่มีคนใช้จ้า อิอิ


## Using ES6 Features

Using `let` and `const` instead of `var`.  
>การประกาศตัวแปรจะใช้ `let` , `const` แทน `var` ทั้งหมด

Here’s the example;

```js
const foo = 1;
let bar = foo;

bar = 9;

console.log(foo, bar); // => 1, 9
```

>Use `const` for all of your references; avoid using `var`.

```js
// bad
var a = 1;
var b = 2;

// good
const a = 1;
const b = 2;
```

>If you must reassign references, use `let` instead of `var`.

```js
// bad
var count = 1;
if (true) {
  count += 1;
}

// good, use the let.
let count = 1;
if (true) {
  count += 1;
}
```

>Note that both `let` and `const` are block-scoped.

```js
// const and let only exist in the blocks they are defined in.
{
  let a = 1;
  const b = 1;
}
console.log(a); // ReferenceError
console.log(b); // ReferenceError
```

## Structure

-   [Angular](./angular-guide.md)
-   [ReactJS](./react-guide.md)
-   [VueJS](./vue-guide.md) **ยังไม่มีคนใช้จ้า อิอิ

## State management

-   [Angular](./angular-guide.md)
-   [ReactJS](./react-guide.md)
-   [VueJS](./vue-guide.md) **ยังไม่มีคนใช้จ้า อิอิ

## Handle Error

``` bra bra bra ```

## Logging

-	The application allow logging on console.log only on the develop environment.

## Tools

 -	The application should logging sentry (https://sentry.io/) for error and some warning.

## Gitworkflow

![enter image description here](../assets/git-workflow.png)

## Authentication // Web session

การใช้ cookie , localStorage ขอไปคุยกับทีมอีกทีนะคะ

``` bra bra bra ```

## Unittest

ขอไปคุยกับทีมอีกทีนะคะ
	 
The code coverage should more than 80%

``` bra bra bra```