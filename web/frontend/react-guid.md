# React guideline

## Package and File Naming

Types for package and name on angular should be using `PascalCase`.

## Enviroment Variable

Environment variable using value is on CI server is not code.

การใช้ environment variable ค่า valueอยู่ที่ CI server แทนที่จะอยู่ใน code

### How to use enviroment variable

For developement we can put enviroment variable in `.env` file and run through variables `process.env.your_env_name` but enviroment variable name should be start with
`REACT_APP_`

Here’s the example

```
REACT_APP_SERVICE_API
```

and should be add enviroment variable `REACT_APP_SERVICE_API` and value at CI.

## Pattern

## React Structure

Using React Redux (\*Reference from toyota project)

```
├── src
│   ├── assets
│   │   ├── styles
│   │   │   └── **/*.scss
│   │   ├── images
│   │   │   ├── **/*.png
│   │   │   ├── icon
│   │   │   │   └── **/*.svg
│   ├── components                   #include layout component
│   │   ├── component_1
│   │   │   ├── components
│   │   │   │   ├── index.ts
│   │   │   │   ├── **/*.tsx
│   │   │   └── index.ts
│   ├── hooks
│   │   ├── **/*.tsx
│   │   └── index.ts
│   ├── constants
│   │   ├── **/*.ts
│   │   └── index.ts
│   ├── store
│   │   ├── **/*.tsx
│   │   └── index.ts
│   ├── pages
│   │   ├── page_1
│   │   │   ├── components
│   │   │   │   ├── styles
│   │   │   │   │   ├── **/*.ts
│   │   │   │   │   ├── index.ts
│   │   │   │   ├── **/*.tsx
│   │   │   │   └── index.ts
│   │   │   └── index.ts
│   │   ├── serivce
│   │   │   └── **/*.ts
│   ├── utils
│   │   ├── **/*.ts
│   │   └── index.ts
│   ├── App.tsx
│   ├── index.tsx
│   ├── react-app-env.d.ts
│   └── serviceWorker.ts
├── utilFunc
│   ├── __tests__
│   └── utilFunc.unit.test.js
│   ├── utilFunc.js
└── index.js
├── config
│   ├── .nginx
├── package.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── manifest.json
│   ├── robots.txt
├── dist (or build)       # gitignore
├── node_modules          # gitignore
├── README.md
├── CHANGELOG.md
├── .env                  # gitignore
├── .env.develop          # gitignore
├── .env.staging          # gitignore
├── .dockerfile
├── .dockerignore
└── .gitignore
```

### File and Folder detail

- **assets** is folder for style (style should be using theme styled-component) and image
- **component** is folder for shared component
  such as button, modal, dialog and can use file and folder.

```
// Good example
  ├── components
  │    ├── Button.ts
  │    ├── Modal
  │    │    ├── Overlay.ts
  │    │    ├── Content.ts
  │    │    └── index.ts
```

- **constants** for storing files for constat variable such as path api, path route

- **store** is folder for Redux

  - action/action creator
  - reducer
  - selector

  Use [Domain-style](https://redux.js.org/faq/code-structure#what-should-my-file-structure-look-like-how-should-i-group-my-action-creators-and-reducers-in-my-project-where-should-my-selectors-go)

- **pages** is folder for component is Page

## Order of import modules

Arranging your ES6 modules in an organized way will save you some time while trying to find any missing/not needed modules.

Preferred :

```js
import { DatePicker } from "../../components";
import axios from "axios";
import { IUser } from "../../models/User";
import React from "react";
import { toCamelCase } from "../utils";
import { Button } from "@material-ui/core";
```

Not Preferred :

```js
// node_modules
import React from "react";
import { Button } from "@material-ui/core";
import axios from "axios";

// Local modules
import { DatePicker } from "../../components";
import { toCamelCase } from "../utils";

// Types + Interfaces
import { IUser } from "../../models/User";
```

## Using destructuring whenever is possible

Another important thing is to prevent unnecessary nesting and repetition. In most of the cases this will improve readability a lot.

Preferred :

```js
const UserProfile = props => (
  <div>
    <span>{props.firstName}</span>
    <span>{props.lastName}</span>
    <img src={props.profilePhoto} />
  </div>
);
```

Not Preferred :

```js
const UserProfile = ({ firstName, lastName, profilePhoto }) => (
  <div>
    <span>{firstName}</span>
    <span>{lastName}</span>
    <img src={profilePhoto} />
  </div>
);
```

## Best practice for build Component

- ใน constuctor มีแค่ไว้กำหนด initial state หรือ property เท่านั้น ถ้านอกเหนือจากนี้ควรไว้ใน componentDidMount แทน

```js
  // bad
  class ListItem extends Compoent{
    constructor() {
      this.state = {
        title: this.props.title
      };
    }
    …
  }

  // good
  class ListItem extends Compoent{
    constructor() {
      this.state = {
        title: this.props.title
      };
    }

    componentDidMount(){
      this.loadData();
    }
    …
  }
```

- componentDidMount ไม่ควรมีโค้ดระดับ implementation ควรจะสร้าง method abstract ก่อนแล้วเรียกใช้ method นั้น

```js
// bad
class ListItem extends Compoent{
  componentDidMount() {
    const { id, fetchData } = this.props;
    fetchData(id);
  }
  …
}

// good
class ListItem extends Compoent{
  componentDidMount() {
    this.fetchItemData();
  }

  fetchItemData = () => {
    const { id, fetchData } = this.props;
    fetchData(id);
  }
  …
}
```

- ใช้ arrow function สำหรับ method เพราะจะได้ไม่ต้อง binding this

```js
 // bad
 class ListItem extends Compoent{
   constructor() {
     // method “onItemClick” need be binded
     this.onItemClick = onItemClick.bind(this);
   }

   onItemClick(){
     …
   }
   …
 }

 // bad
 class ListItem extends Compoent{
   constructor() {
     // method “onItemClick” doesn’t need be binded anymore
   }

   onItemClick = () =>{
     …
   }
   …
 }
```

- ต้องสร้าง ตัวแปรเพื่อเก็บ propTypes และ defaultProps แล้วค่อย assign ให้กับ component

```js
// good
export const TimeCardPropTypes = {
  currentDate: propTypes.object,
  onUpdateClick: propTypes.func,
  onDateChange: propTypes.func
};

export const currentDateDefaultProps = {
  currentDate: moment().toDate()
};

TimeCard.propTypes = TimeCardPropTypes;
TimeCard.defaultProps = currentDateDefaultProps;

// bad
TimeCard.propTypes = {
  currentDate: propTypes.object,
  onUpdateClick: propTypes.func,
  onDateChange: propTypes.func
};
TimeCard.defaultProps = {
  currentDate: moment().toDate()
};
```

สาเหตุที่ต้องสร้างตัวแปรเก็บไว้ก่อนนั้นก็เพื่อง่ายต่อการ export เพื่อให้ component อื่นๆที่ต้องการใช้ propTypes สามารถ import ได้ (extending)

ต้อง export propTypes และ defaultProps เสมอ
