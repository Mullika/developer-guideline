# Angular guideline


## Should be angular cli and using angular cli for generate file.

installing angular cli 

```
npm install -g @angular/cli
```

## Package and File Naming

Types for package and file name on angular should be using `kebab-case`

## Code Structure

```
|-- app
     |-- pages
       |-- home
           |-- [+] components
           |-- [+] pages
           |-- home-routing.module.ts
           |-- home.module.ts
     |-- core
       |-- [+] authentication
       |-- [+] footer 
       |-- [+] guards
       |-- [+] http
       |-- [+] interceptors
       |-- [+] mocks
       |-- [+] services
       |-- [+] header
       |-- core.module.ts
       |-- ensureModuleLoadedOnceGuard.ts
       |-- logger.service.ts
     |
     |-- shared
          |-- [+] components
          |-- [+] directives
          |-- [+] pipes
          |-- [+] models
     |
     |-- [+] configs
|-- assets
     |-- scss
          |-- [+] partials
          |-- _base.scss
          |-- styles.scss
```


## Lazy loading

Lazy loading the modules can enhance your productivity. Lazy Load is a built-in feature in Angular which helps developers with loading the thing which is required. For example, when you use the feature, it loads the components and other things you needed and stops other and unnecessary files to get loaded. 

Here’s how it functions;
```js
const appRoutes: Route[] = [
  { path: 'home', component: HomeComponent },
  { path: 'second-page', loadChildren: "./second-page/second-page.module#SecondPageModule" },
]
```


## State Management



![enter image description here](../assets/ngrx.png)

`https://ngrx.io/guide/store`

Installing ngrx

```
npm install @ngrx/{store,store-devtools,entity,effects}
```

### **ngrx structure**

```
     ├── app
     │ ├── app-routing.module.ts
     │ ├── app.component.css
     │ ├── app.component.html
     │ ├── app.component.ts
     │ ├── app.module.ts
     │ ├── shared
     │ ├── pages
     │ │    └── my-feature
     │ │         ├── my-feature.component.css
     │ │         ├── my-feature.component.html
     │ │         └── my-feature.component.ts
     │ ├── models
     │ │    ├── index.ts
     │ │    └── my-model.ts
     │ │    └── user.ts
     │ ├── root-store
     │ │    ├── index.ts
     │ │    ├── root-store.module.ts
     │ │    ├── selectors.ts
     │ │    ├── state.ts
     │ │    └── my-feature-store
     │ │    |    ├── actions.ts
     │ │    |    ├── effects.ts
     │ │    |    ├── index.ts
     │ │    |    ├── reducer.ts
     │ │    |    ├── selectors.ts
     │ │    |    ├── state.ts
     │ │    |    └── my-feature-store.module.ts
     │ │    └── my-other-feature-store
     │ │         ├── actions.ts
     │ │         ├── effects.ts
     │ │         ├── index.ts
     │ │         ├── reducer.ts
     │ │         ├── selectors.ts
     │ │         ├── state.ts
     │ │         └── my-other-feature-store.module.ts
     │ └── services
     │      └── data.service.ts
     ├── assets
     ├── browserslist
     ├── environments
     │ ├── environment.prod.ts
     │ └── environment.ts
     ├── index.html
     ├── main.ts
     ├── polyfills.ts
     ├── styles.css
     ├── test.ts
     ├── tsconfig.app.json
     ├── tsconfig.spec.json
     └── tslint.json
```

`https://itnext.io/ngrx-best-practices-for-enterprise-angular-applications-6f00bcdf36d7`






